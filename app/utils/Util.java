package utils;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

public class Util {
    public static ObjectNode createResponse(Object response, boolean ok) {
        ObjectNode result = Json.newObject();
        result.put("isSuccessful", ok);
        if (response instanceof String) {
            result.put("data", (String) response);
        } else {
            result.putPOJO("data", response);
        }
        return result;
    }
}
