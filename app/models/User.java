package models;

import io.ebean.Model;
import io.ebean.Finder;
import io.ebean.Ebean;
import io.ebean.Query;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")

public class User extends Model {
    @Id
    @Column
    private Integer id;

    @Column
    private String fullname;

    @Column
    private String email;

    @Column
    private String address;

    @Column
    private String password;

    public static Finder<Integer, User> find = new Finder<Integer, User>(User.class);

    public Integer getId() {
        return id;
    }
    public String getFullname() {
        return fullname;
    }

    public String getEmail(){
        return email;
    }

    public String getAddress(){
        return address;
    }

}