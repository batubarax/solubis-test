package controllers;
import play.mvc.*;
import play.libs.Json;
import java.util.List;
import javax.inject.Inject;
import models.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import utils.Util;

public class UserController extends Controller{

    public Result index(){ 
        List<User> users = User.find.query().findList();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonData = mapper.convertValue(users, JsonNode.class);

        return ok(Util.createResponse(jsonData, true));
    }

    public Result user(Integer id){
        User user = User.find.byId(id);

        if (null == user) {
            return notFound(Util.createResponse("User Not Found", false));
        }

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonData = mapper.convertValue(user, JsonNode.class);

        return ok(Util.createResponse(jsonData, true));
    }



}
